import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/manager'
  },

  // user url
  {
    path: '/user',
    name: 'user home',
    component: () => import('../views/user/UserHome.vue')
  },
  {
    path: '/user/menu',
    name: 'menu display',
    component: () => import('../views/user/MenuView.vue')
  },
  {
    path: '/user/trolley',
    name: 'shopping trolley',
    component: () => import('../views/user/TrolleyView.vue')
  },
  {
    path: '/user/order',
    name: 'order',
    component: () => import('../views/user/OrderView.vue')
  },
  {
    path: '/user/account',
    name: 'account information',
    component: () => import('../views/user/AccountView.vue')
  },
  {
    path: '/user/address',
    name: 'address management',
    component: () => import('../views/user/AddressView.vue')
  },
  {
    path: '/user/favorite',
    name: 'favorite',
    component: () => import('../views/user/FavoriteView.vue')
  },
  {
    path: '/user/unlike',
    name: 'unlike',
    component: () => import('../views/user/UnlikeView.vue')
  },

  // manager url
  {
    path: '/manager',
    name: 'manager home',
    component: () => import('../views/manager/ManagerHome.vue')
  },
  {
    path: '/manager/category',
    name: 'category management',
    component: () => import('../views/manager/CategoryManagement.vue')
  },
  {
    path: '/manager/product',
    name: 'product management',
    component: () => import('../views/manager/ProductManagement.vue')
  },
  {
    path: '/manager/order',
    name: 'order details',
    component: () => import('../views/manager/OrderDetails.vue')
  },
  {
    path: '/manager/refund',
    name: 'refund handle',
    component: () => import('../views/manager/RefundHandle.vue')
  },
  {
    path: '/manager/income',
    name: 'income report',
    component: () => import('../views/manager/IncomeReport.vue')
  },
  {
    path: '/manager/sales',
    name: 'sales report',
    component: () => import('../views/manager/SalesReport.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
